# Set up TFTP server which can download and upload
## Download should be fine for all, but upload file to TFTP server could be a problem
the package should use "atftpd" not tftpd

## Step 1
sudo apt-get install tftp atftpd xinetd

## Step 2
sudo mkdir /tftpboot
sudo chown -R nobody /tftpboot
sudo chmod 777 /tftpboot

## Step 2
setting file
/etc/xinetd.d/tftp

service tftp
{
    protocol = udp 
    port = 69
    socket_type = dgram
    wait = yes 
    user = nobody
    server = /usr/sbin/in.tftpd
    server_args = /tftpboot
    disable = no
    flags = IPv4
}

## step 3
sudo systemctl restart xinetd.service

## Step 4
test
in the same PC.

tftp localhost
get filename
put filename

